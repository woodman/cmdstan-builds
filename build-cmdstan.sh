#!/bin/bash

# ver=2.28.2
set -eu
fname=cmdstan-${ver}.tar.gz
url=https://github.com/stan-dev/cmdstan/releases/download/v${ver}/$fname
curl -LO $url
tar xzf $fname
rm $fname
cd cmdstan-$ver
make build
cd ..
tar czf cmdstan-${ver}-build.tar.gz2 cmdstan-${ver}
