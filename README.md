# CmdStan builds

[CmdStan](https://github.com/stan-dev/cmdstan) is a software for
Bayesian modeling, which is written in C++ and thus a bit onerous
to build in the collaboratory.  To facilitate it's use, this project
prebuilds CmdStan for use elsewhere.
